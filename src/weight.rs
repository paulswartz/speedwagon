use crate::link::{Link, LinkKind, OneWay};

pub trait Weight {
    fn for_link(&self, link: &Link) -> Option<usize> {
        match link.one_way {
            OneWay::YesOther => None,
            _ => self.for_kind(link.kind),
        }
    }

    fn for_kind(&self, kind: LinkKind) -> Option<usize>;

    fn max(&self) -> usize;
}

pub struct Walk {}

impl Weight for Walk {
    fn max(&self) -> usize {
        6
    }

    fn for_link(&self, link: &Link) -> Option<usize> {
        // don't care about one-way for walking
        self.for_kind(link.kind)
    }

    fn for_kind(&self, kind: LinkKind) -> Option<usize> {
        match kind {
            LinkKind::Primary => Some(5),
            LinkKind::Secondary => Some(5),
            LinkKind::Unclassified => Some(5),
            LinkKind::Footway => Some(6),
            LinkKind::Cycleway => Some(1),
            LinkKind::Service => Some(5),
            _ => None,
        }
    }
}
