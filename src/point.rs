use geo::prelude::*;
use geo::{Coordinate, LineString};

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Point {
    pub latitude: f64,
    pub longitude: f64,
}

impl Point {
    pub fn min() -> Self {
        Self {
            latitude: std::f64::MIN,
            longitude: std::f64::MIN,
        }
    }

    pub fn max() -> Self {
        Self {
            latitude: std::f64::MAX,
            longitude: std::f64::MAX,
        }
    }

    pub fn to_coordinate(&self) -> Coordinate<f64> {
        Coordinate {
            x: self.longitude,
            y: self.latitude,
        }
    }

    pub fn to_geo_point(&self) -> geo::Point<f64> {
        geo::Point::new(self.longitude, self.latitude)
    }

    pub fn distance(&self, b: &Point) -> f64 {
        let line: LineString<f64> = LineString::from(vec![self.to_coordinate(), b.to_coordinate()]);
        line.haversine_length()
    }

    pub fn bearing(&self, b: &Point) -> f64 {
        self.to_geo_point().bearing(b.to_geo_point())
    }

    pub fn int_distance(&self, b: &Point) -> u64 {
        let meters = self.distance(b);
        (meters * 1000_f64) as u64
    }

    pub fn parse(s: &str) -> Option<Self> {
        let parts: Vec<&str> = s.split(',').collect();
        if parts.len() != 2 {
            return None;
        }
        let latitude: f64 = parts.get(0)?.parse().ok()?;
        let longitude: f64 = parts.get(1)?.parse().ok()?;
        Some(Point {
            latitude,
            longitude,
        })
    }
}

impl rstar::Point for Point {
    type Scalar = f64;
    const DIMENSIONS: usize = 2;

    fn generate(generator: impl Fn(usize) -> Self::Scalar) -> Self {
        Point {
            latitude: generator(0),
            longitude: generator(1),
        }
    }

    fn nth(&self, index: usize) -> Self::Scalar {
        match index {
            0 => self.latitude,
            1 => self.longitude,
            _ => unreachable!(),
        }
    }

    fn nth_mut(&mut self, index: usize) -> &mut Self::Scalar {
        match index {
            0 => &mut self.latitude,
            1 => &mut self.longitude,
            _ => unreachable!(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse() {
        let str = "42.33494576417536,-71.03982925415038";
        let expected = Some(Point {
            latitude: 42.334_945_764_175_36,
            longitude: -71.039_829_254_150_38,
        });
        let actual = Point::parse(str);
        assert_eq!(expected, actual);

        assert_eq!(None, Point::parse("1.234"));
        assert_eq!(None, Point::parse("1.223,"));
        assert_eq!(None, Point::parse("x,y"))
    }
}
