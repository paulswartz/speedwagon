use log::info;
use nickel::{HttpRouter, Nickel, StaticFilesHandler};
use regex::Regex;

fn main() {
    env_logger::init();
    info!("starting up...");
    let mut nickel_router = Nickel::router();
    nickel_router.get("/otp/routers/:name", speedwagon::Router::index);
    nickel_router.get("/otp/routers/:name/plan", speedwagon::Router::plan);
    nickel_router.get(
        "/otp/routers/:name/index/routes",
        speedwagon::Router::index_routes,
    );
    nickel_router.get(
        "/otp/routers/:name/index/stops",
        speedwagon::Router::index_stops,
    );
    nickel_router.get(
        Regex::new(
            "^/otp/routers/(?P<name>[a-zA-Z0-0 -_]+)/index/stops/(?P<id>[a-zA-Z0-9 -_:]+)/routes/?",
        )
        .unwrap(),
        speedwagon::Router::index_routes_at_stop,
    );
    nickel_router.get(
        Regex::new(
            "^/otp/routers/(?P<name>[a-zA-Z0-0 -_]+)/index/stops/(?P<id>[a-zA-Z0-9 -_:]+)/?$",
        )
        .unwrap(),
        speedwagon::Router::get_stops,
    );
    let args: Vec<_> = std::env::args_os().collect();
    info!("building map...");
    let map = speedwagon::StreetMap::from_pbf(&args[1]);
    info!("building gtfs...");
    let gtfs = gtfs_structures::Gtfs::new(&args[2].to_str().unwrap()).unwrap();
    gtfs.print_stats();
    let router = speedwagon::Router::new(map, gtfs);
    info!("starting server...");
    let mut server = Nickel::with_data(router);
    server.utilize(nickel_router);
    server.utilize(StaticFilesHandler::new("./client/"));

    server.listen("0.0.0.0:8080").unwrap();
}
