use crate::{Link, LinkKind, NodeId, OneWay, Point, Weight};
use log::{debug, info, warn};
use petgraph::graphmap::DiGraphMap;
use petgraph::visit::{EdgeRef, VisitMap, Visitable};
use rstar::{primitives::PointWithData, RTree};
use std::collections::hash_map::Entry::{Occupied, Vacant};

pub struct StreetMap {
    nodes: fnv::FnvHashMap<NodeId, Point>,
    rtree: rstar::RTree<PointWithData<NodeId, Point>, rstar::DefaultParams>,
    pub min: Point,
    pub max: Point,
    graph: DiGraphMap<NodeId, Link>,
}

struct Builder {
    all_nodes: fnv::FnvHashMap<NodeId, Point>,
    graph: DiGraphMap<NodeId, Link>,
}

impl StreetMap {
    pub fn from_pbf(filename: &std::ffi::OsStr) -> Self {
        let r =
            std::io::BufReader::new(std::fs::File::open(&std::path::Path::new(filename)).unwrap());
        let mut pbf = osmpbfreader::OsmPbfReader::new(r);
        let mut builder = Builder::new();
        for obj in pbf.par_iter() {
            match obj {
                Ok(osmpbfreader::OsmObj::Node(node)) => {
                    let point = Point {
                        latitude: node.lat(),
                        longitude: node.lon(),
                    };
                    builder.insert_node(node.id.0, point);
                }
                Ok(osmpbfreader::OsmObj::Way(way)) => match way.tags.get("highway") {
                    None => {}
                    Some(highway) => {
                        let first = way.nodes.iter();
                        let mut second = way.nodes.iter();
                        second.next();
                        debug!("{}", way.id.0);
                        let name = way.tags.get("name");
                        let one_way = OneWay::from_tag(way.tags.get("oneway").map(|x| &x[..]));
                        let kind = LinkKind::from_highway(highway);
                        for key in way.tags.keys() {
                            debug!("    {}", key);
                        }
                        for (fst, snd) in first.zip(second) {
                            debug!("{} -> {}", fst.0, snd.0);
                            let link = Link {
                                from: fst.0,
                                to: snd.0,
                                name: name.cloned(),
                                one_way,
                                kind,
                            };
                            builder.insert_edge(fst.0, snd.0, link);
                            // reverse
                            let link = Link {
                                from: snd.0,
                                to: fst.0,
                                name: name.cloned(),
                                one_way: one_way.reverse(),
                                kind,
                            };
                            builder.insert_edge(snd.0, fst.0, link);
                        }
                    }
                },
                Ok(osmpbfreader::OsmObj::Relation(_rel)) => {}
                Err(_err) => {}
            }
        }
        builder.build()
    }

    fn id_distance(&self, from: NodeId, to: NodeId) -> u64 {
        let from = self.nodes.get(&from).unwrap();
        let to = self.nodes.get(&to).unwrap();
        from.int_distance(&to)
    }

    pub fn point(&self, id: NodeId) -> Point {
        *self.nodes.get(&id).unwrap()
    }

    pub fn closest(&self, point: &Point) -> (NodeId, Point) {
        let pwd = self.rtree.nearest_neighbor(point).unwrap();
        (pwd.data, *pwd.position())
    }

    pub fn calculate_route(
        &self,
        from: NodeId,
        to: NodeId,
        weight: Box<dyn Weight>,
    ) -> Option<Vec<Link>> {
        let weight_max = weight.max() as u64;
        let mut queue = priority_queue::PriorityQueue::new();
        let mut scores = fnv::FnvHashMap::default();
        let mut seen_nodes = self.graph.visit_map();
        let mut paths: fnv::FnvHashMap<NodeId, NodeId> = fnv::FnvHashMap::default();

        let initial_estimate = self.id_distance(from, to) / weight_max;
        scores.insert(from, 0);
        queue.push(from, std::cmp::Reverse(initial_estimate));
        warn!("planning from {} to {}...", from, to);
        while let Some((a, _)) = queue.pop() {
            info!(
                "trying {}, score {}, remaining {}",
                a,
                scores[&a],
                self.id_distance(a, to)
            );
            if a == to {
                let mut node = a;
                let mut links = vec![];
                while let Some(&parent) = paths.get(&node) {
                    let link = self.graph.edge_weight(parent, node).unwrap();
                    links.push(link.clone());
                    node = parent;
                    if node == from {
                        break;
                    }
                }
                links.reverse();
                return Some(links);
            }

            if !seen_nodes.visit(a) {
                continue;
            }
            let a_score = scores[&a];
            for edge in self.graph.edges(a) {
                let b = edge.target();
                if seen_nodes.is_visited(&b) {
                    continue;
                }
                let link = edge.weight();
                if let Some(weight) = weight.for_link(link) {
                    let distance = self.id_distance(a, b);
                    let weighted_distance = distance / weight as u64;
                    let mut b_score = a_score + weighted_distance;
                    let estimated_remaining = self.id_distance(b, to) / weight_max;
                    match scores.entry(b) {
                        Occupied(ent) => {
                            let old_score = *ent.get();
                            if b_score < old_score {
                                *ent.into_mut() = b_score;
                                paths.insert(b, a);
                            } else {
                                b_score = old_score;
                            }
                        }
                        Vacant(ent) => {
                            ent.insert(b_score);
                            paths.insert(b, a);
                        }
                    }

                    let next_estimate = b_score + estimated_remaining;
                    info!(
                        "{:?} {} {} {} {}",
                        link, distance, weight, b_score, next_estimate
                    );
                    queue.push(b, std::cmp::Reverse(next_estimate));
                }
            }
        }

        None
    }
}

impl Builder {
    fn new() -> Self {
        let all_nodes = fnv::FnvHashMap::default();
        let graph = DiGraphMap::new();
        Self { all_nodes, graph }
    }

    fn insert_node(&mut self, node_id: NodeId, point: Point) {
        self.all_nodes.insert(node_id, point);
    }

    fn insert_edge(&mut self, from_id: NodeId, to_id: NodeId, link: Link) {
        self.graph.add_edge(from_id, to_id, link);
    }

    fn build(self) -> StreetMap {
        let mut nodes: fnv::FnvHashMap<NodeId, Point> =
            fnv::FnvHashMap::with_capacity_and_hasher(self.graph.node_count(), Default::default());
        let rtree = RTree::bulk_load(
            self.graph
                .nodes()
                .map(|id| {
                    let point = *self.all_nodes.get(&id).unwrap();
                    let pwd = PointWithData::new(id, point);
                    nodes.insert(id, point);
                    pwd
                })
                .collect(),
        );
        let min_max = rtree.root().envelope();
        StreetMap {
            nodes,
            rtree,
            min: min_max.lower(),
            max: min_max.upper(),
            graph: self.graph,
        }
    }
}
