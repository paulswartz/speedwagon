pub type NodeId = i64;

#[derive(Debug, Clone)]
pub struct Link {
    pub from: NodeId,
    pub to: NodeId,
    pub name: Option<String>,
    pub one_way: OneWay,
    pub kind: LinkKind,
}

#[derive(Debug, Clone, Copy)]
pub enum OneWay {
    Yes,
    YesOther,
    No,
}

impl OneWay {
    pub fn from_tag(tag: Option<&str>) -> Self {
        match tag {
            Some("yes") => Self::Yes,
            Some("-1") => Self::YesOther,
            _ => Self::No,
        }
    }

    pub fn reverse(self) -> Self {
        match self {
            Self::Yes => Self::YesOther,
            Self::YesOther => Self::Yes,
            Self::No => Self::No,
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum LinkKind {
    Primary,
    Secondary,
    Unclassified,
    Footway,
    Cycleway,
    Service,
    NotOpen,
}

impl LinkKind {
    pub fn from_highway(highway: &str) -> LinkKind {
        match highway {
            "primary" => Self::Primary,
            "primary_link" => Self::Primary,
            "trunk" => Self::Primary,
            "trunk_link" => Self::Primary,
            "motorway" => Self::Primary,
            "motorway_link" => Self::Primary,
            "raceway" => Self::Primary,
            "yes" => Self::Primary,
            "secondary" => Self::Secondary,
            "secondary_link" => Self::Secondary,
            "tertiary" => Self::Secondary,
            "tertiary_link" => Self::Secondary,
            "rest_area" => Self::Secondary,
            "services" => Self::Secondary,
            "access_ramp" => Self::Secondary,
            "footway" => Self::Footway,
            "path" => Self::Footway,
            "track" => Self::Footway,
            "pedestrian" => Self::Footway,
            "steps" => Self::Footway,
            "bridleway" => Self::Footway,
            "track;path" => Self::Footway,
            "elevator" => Self::Footway,
            "corridor" => Self::Footway,
            "crossing" => Self::Footway,
            "bus_stop" => Self::Footway,
            "service" => Self::Service,
            "unclassified" => Self::Unclassified,
            "residential" => Self::Unclassified,
            "living_street" => Self::Unclassified,
            "junction" => Self::Unclassified,
            "road" => Self::Unclassified,
            "abandoned" => Self::NotOpen,
            "construction" => Self::NotOpen,
            "disused" => Self::NotOpen,
            "escape" => Self::NotOpen,
            "proposed" => Self::NotOpen,
            "cycleway" => Self::Cycleway,
            _ => panic!("unknown highway: {}", highway),
        }
    }
}
