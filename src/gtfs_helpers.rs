use gtfs_structures::Gtfs;
use std::collections::{HashMap, HashSet};

pub struct GtfsHelpers {
    routes_at_stop: HashMap<String, HashSet<String>>,
}

impl GtfsHelpers {
    pub fn new(gtfs: &Gtfs) -> Self {
        let mut routes_at_stop = HashMap::new();
        for trip in gtfs.trips.values() {
            for stop_time in trip.stop_times.iter() {
                routes_at_stop
                    .entry(stop_time.stop.id.clone())
                    .or_insert_with(HashSet::new)
                    .insert(trip.route_id.clone());
            }
        }

        Self { routes_at_stop }
    }

    pub fn route_ids_at_stop_id(&self, stop_id: &str) -> Vec<&String> {
        match self.routes_at_stop.get(stop_id) {
            Some(routes) => routes.iter().collect(),
            None => Vec::new(),
        }
    }
}
