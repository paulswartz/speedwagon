use crate::{
    gtfs_helpers::GtfsHelpers, itinerary::Itinerary, weight::Walk, Link, Point, StreetMap,
};
use chrono::{DateTime, NaiveDate, NaiveTime, TimeZone, Utc};
use chrono_tz::US::Eastern;
use gtfs_structures::{Gtfs, Route, RouteType};
use nickel::{status::StatusCode, MiddlewareResult, Params, QueryString, Request, Response};
use serde_json::json;

pub struct Router {
    map: StreetMap,
    gtfs: Gtfs,
    gtfs_helpers: GtfsHelpers,
}

struct PlanRequest {
    from: Point,
    to: Point,
    date_time: DateTime<Utc>,
}

enum PlanRequestError {
    UnableToPlan,
    MissingField(&'static str),
}

impl PlanRequest {
    fn from_query(query: &Params) -> Result<Self, PlanRequestError> {
        let from = Self::point(query, "fromPlace", "from")?;
        let to = Self::point(query, "toPlace", "to")?;
        let date_time = Self::date_time(query)?;
        Ok(Self {
            from,
            to,
            date_time,
        })
    }

    fn point(
        query: &Params,
        param: &str,
        missing: &'static str,
    ) -> Result<Point, PlanRequestError> {
        match query.get(param).and_then(Point::parse) {
            Some(p) => Ok(p),
            _ => Err(PlanRequestError::MissingField(missing)),
        }
    }

    fn date_time(query: &Params) -> Result<DateTime<Utc>, PlanRequestError> {
        let date = query
            .get("date")
            .ok_or(PlanRequestError::MissingField("date"))?;
        let time = query
            .get("time")
            .ok_or(PlanRequestError::MissingField("time"))?;
        Self::parse_date_time(date, time).ok_or(PlanRequestError::MissingField("date"))
    }

    fn parse_date_time(date: &str, time: &str) -> Option<DateTime<Utc>> {
        let date = NaiveDate::parse_from_str(date, "%m-%d-%Y").ok()?;
        let time = NaiveTime::parse_from_str(time, "%-I:%M%P").ok()?;
        Eastern
            .from_local_date(&date)
            .and_time(time)
            .earliest()
            .map(|dt| dt.with_timezone(&Utc))
    }
}

trait RouterJson {
    fn into_json(&self) -> serde_json::Value;
}

impl RouterJson for Route {
    fn into_json(&self) -> serde_json::Value {
        let mode = match self.route_type {
            RouteType::Tramway => "TRAM",
            RouteType::Subway => "SUBWAY",
            RouteType::Bus => "BUS",
            RouteType::CableCar => "CABLECAR",
            RouteType::Ferry => "FERRY",
            RouteType::Funicular => "FUNICULAR",
            RouteType::Gondola => "GONDOLA",
            RouteType::Rail => "RAIL",
            RouteType::Other(_) => "OTHER",
        };
        json!({
            "id": self.id,
            "shortName": self.short_name,
            "longName": self.long_name,
            "mode": mode,
            "color": self.route_color.map(|c| format!("{:02X}{:02X}{:02X}", c.r, c.g, c.b)),
            "agency_name": "TODO"
        })
    }
}

impl RouterJson for gtfs_structures::Stop {
    fn into_json(&self) -> serde_json::Value {
        json!({
            "id": format!("1:{}", self.id),
            "code": self.code,
            "name": self.name,
            "lat": self.latitude.unwrap(),
            "lon": self.longitude.unwrap(),
            //"zone_id": self.zone_id,
            //"url": self.url,
            "locationType": self.location_type as u8,
            "wheelchairBoarding": self.wheelchair_boarding as u8,
            //"vehicleType": self.vehicle_type,
            //"vehicleTypeSet": self.vehicle_type.is_some(),
            "cluster": self.parent_station
        })
    }
}

impl<T> RouterJson for &T
where
    T: RouterJson,
{
    fn into_json(&self) -> serde_json::Value {
        RouterJson::into_json(*self)
    }
}

impl<T> RouterJson for std::sync::Arc<T>
where
    T: RouterJson,
{
    fn into_json(&self) -> serde_json::Value {
        RouterJson::into_json(self.as_ref())
    }
}

impl Router {
    pub fn new(map: StreetMap, gtfs: Gtfs) -> Self {
        let gtfs_helpers = GtfsHelpers::new(&gtfs);
        Self {
            map,
            gtfs,
            gtfs_helpers,
        }
    }

    pub fn index<'mw>(
        _req: &mut Request<Self>,
        res: Response<'mw, Self>,
    ) -> MiddlewareResult<'mw, Self> {
        let router = res.server_data();
        let value = json!({
            "lowerLeftLongitude": router.map.min.longitude,
            "lowerLeftLatitude": router.map.min.latitude,
            "upperRightLongitude": router.map.max.longitude,
            "upperRightLatitude": router.map.max.latitude,
            "centerLongitude": (router.map.max.longitude + router.map.min.longitude) / 2.0,
            "centerLatitude": (router.map.max.latitude + router.map.max.latitude) / 2.0,
            "transitModes": [],
            "travelOptions": [
                {
                    "value": "WALK",
                    "name": "WALK"
                }
            ],
        });
        res.send(value)
    }

    pub fn index_routes<'mw>(
        _req: &mut Request<Self>,
        res: Response<'mw, Self>,
    ) -> MiddlewareResult<'mw, Self> {
        let router = res.server_data();
        let values: Vec<serde_json::Value> = router
            .gtfs
            .routes
            .values()
            .map(RouterJson::into_json)
            .collect();
        let value: serde_json::Value = serde_json::to_value(values).unwrap();
        res.send(value)
    }

    pub fn index_routes_at_stop<'mw>(
        req: &mut Request<Self>,
        res: Response<'mw, Self>,
    ) -> MiddlewareResult<'mw, Self> {
        let id = match req.param("id") {
            Some(id) => &id[2..],
            None => {
                return res.send(StatusCode::NotFound);
            }
        };
        let router = res.server_data();
        let values: Vec<serde_json::Value> = router
            .gtfs_helpers
            .route_ids_at_stop_id(&id)
            .iter()
            .map(|route_id| router.gtfs.routes.get(route_id.as_str()).unwrap())
            .map(RouterJson::into_json)
            .collect();
        let value: serde_json::Value = serde_json::to_value(values).unwrap();
        res.send(value)
    }

    pub fn index_stops<'mw>(
        req: &mut Request<Self>,
        res: Response<'mw, Self>,
    ) -> MiddlewareResult<'mw, Self> {
        let query = req.query();
        let max_lat = query.get("maxLat").and_then(|x| x.parse().ok());
        let max_lon = query.get("maxLon").and_then(|x| x.parse().ok());
        let min_lat = query.get("minLat").and_then(|x| x.parse().ok());
        let min_lon = query.get("minLon").and_then(|x| x.parse().ok());
        match (max_lat, max_lon, min_lat, min_lon) {
            (Some(max_lat), Some(max_lon), Some(min_lat), Some(min_lon)) => {
                let router = res.server_data();
                let stops: Vec<serde_json::Value> = router
                    .gtfs
                    .stops
                    .values()
                    .filter(|stop| match (stop.latitude, stop.longitude) {
                        (Some(lat), Some(lon)) => {
                            lat >= min_lat && lat <= max_lat && lon >= min_lon && lon <= max_lon
                        }
                        _ => false,
                    })
                    .map(RouterJson::into_json)
                    .collect();
                let value: serde_json::Value = serde_json::to_value(stops).unwrap();
                res.send(value)
            }
            _ => res.next_middleware(),
        }
    }

    pub fn get_stops<'mw>(
        req: &mut Request<Self>,
        res: Response<'mw, Self>,
    ) -> MiddlewareResult<'mw, Self> {
        let id = match req.param("id") {
            Some(id) => &id[2..],
            None => {
                return res.send(StatusCode::NotFound);
            }
        };
        let router = res.server_data();
        let stop = match router.gtfs.stops.get(id) {
            Some(stop) => stop,
            None => {
                return res.send(StatusCode::NotFound);
            }
        };
        res.send(stop.into_json())
    }

    pub fn plan<'mw>(
        req: &mut Request<Self>,
        res: Response<'mw, Self>,
    ) -> MiddlewareResult<'mw, Self> {
        let query = req.query();
        let router = res.server_data();
        let plan_req = match PlanRequest::from_query(query) {
            Ok(plan_req) => plan_req,
            Err(err) => {
                return router.plan_error(res, err);
            }
        };
        let itineraries = match router.map.calculate_route(
            router.map.closest(&plan_req.from).0,
            router.map.closest(&plan_req.to).0,
            Box::new(Walk {}),
        ) {
            Some(node_ids) => vec![router.itinerary_value(&plan_req.date_time, &node_ids)],
            None => return router.plan_error(res, PlanRequestError::UnableToPlan),
        };
        let value = json!({
            "plan": {
                "date": plan_req.date_time.timestamp_millis(),
                "from": {
                    "lat": plan_req.from.latitude,
                    "lon": plan_req.from.longitude,
                },
                "to": {
                    "lat": plan_req.to.latitude,
                    "lon": plan_req.to.longitude,
                },
                "itineraries": itineraries,
            }
        });
        res.send(value)
    }

    fn plan_error<'mw>(
        &self,
        mut res: Response<'mw, Self>,
        err: PlanRequestError,
    ) -> MiddlewareResult<'mw, Self> {
        res.set(StatusCode::BadRequest);
        let body = match err {
            PlanRequestError::UnableToPlan => json!({"msg": "unable to plan"}),
            PlanRequestError::MissingField(field) => json!({ "missing": [field] }),
        };
        let value = json!({ "error": body });
        res.send(value)
    }

    fn itinerary_value(&self, start_time: &DateTime<Utc>, links: &[Link]) -> serde_json::Value {
        let value = Itinerary::from_links(&self.map, start_time, links);
        serde_json::to_value(value).unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::{DateTime, Utc};

    #[test]
    fn test_request_parse_date_time() {
        assert_eq!(
            PlanRequest::parse_date_time("02-26-2020", "2:32pm"),
            DateTime::parse_from_rfc3339("2020-02-26T19:32:00Z")
                .map(DateTime::<Utc>::from)
                .ok()
        )
    }
}
