use crate::{Link, Point, StreetMap};
use chrono::{DateTime, Utc};
use serde::Serialize;

#[derive(Serialize)]
pub struct Itinerary {
    #[serde(rename = "startTime")]
    start_time: i64,
    #[serde(rename = "endTime")]
    end_time: i64,
    duration: i64,
    #[serde(rename = "walkDistance")]
    walk_distance: f64,
    #[serde(rename = "walkTime")]
    walk_time: i64,
    legs: Vec<Leg>,
}

impl Itinerary {
    pub fn from_links(map: &StreetMap, start_time: &DateTime<Utc>, links: &[Link]) -> Self {
        let legs: Vec<Leg> = vec![Leg::from_links(map, start_time, links)];
        let walk_distance = legs.iter().map(|s| s.distance).sum();
        let start_time = legs[0].start_time;
        let end_time = legs[legs.len() - 1].end_time;
        let duration = legs.iter().map(|s| s.duration).sum();
        Self {
            walk_distance,
            start_time,
            end_time,
            duration,
            walk_time: duration,
            legs,
        }
    }
}

#[derive(Serialize)]
struct Leg {
    #[serde(rename = "startTime")]
    start_time: i64,
    #[serde(rename = "endTime")]
    end_time: i64,
    duration: i64,
    distance: f64,
    from: Vertex,
    to: Vertex,
    mode: ModeType,
    #[serde(rename = "legGeometry")]
    geometry: Geometry,
    steps: Vec<Step>,
}

impl Leg {
    fn from_links(map: &StreetMap, start_time: &DateTime<Utc>, links: &[Link]) -> Self {
        let steps: Vec<Step> = Step::from_links(map, links);
        let mut points: Vec<Point> = links
            .iter()
            .flat_map(|l| vec![map.point(l.from), map.point(l.to)])
            .collect();
        points.dedup();
        let coordinates: Vec<geo::Coordinate<f64>> =
            points.iter().map(|x| x.to_coordinate()).collect();

        let geometry = Geometry {
            length: coordinates.len(),
            points: polyline::encode_coordinates(coordinates, 5).unwrap(),
        };
        let from = Vertex::from_point("Origin", &points[0]);
        let to = Vertex::from_point("Destination", &points[points.len() - 1]);
        let distance = steps.iter().map(|s| s.distance).sum();
        let duration = (distance / 1.4) as i64;
        Self {
            steps,
            geometry,
            from,
            to,
            mode: ModeType::WALK,
            distance,
            start_time: start_time.timestamp_millis(),
            end_time: start_time.timestamp_millis() + (duration * 1000),
            duration,
        }
    }
}

#[derive(Serialize)]
enum ModeType {
    WALK,
}

#[derive(Serialize)]
struct Vertex {
    name: String,
    lon: f64,
    lat: f64,
}

impl Vertex {
    fn from_point(name: &str, point: &Point) -> Self {
        Self {
            name: name.to_string(),
            lon: point.longitude,
            lat: point.latitude,
        }
    }
}

#[derive(Serialize)]
struct Geometry {
    points: String,
    length: usize,
}

#[derive(Serialize)]
struct Step {
    distance: f64,
    #[serde(rename = "relativeDirection")]
    relative_direction: RelativeDirection,
    #[serde(rename = "absoluteDirection")]
    absolute_direction: AbsoluteDirection,
    #[serde(rename = "streetName")]
    street_name: String,
    #[serde(rename = "bogusName")]
    bogus_name: bool,
    lon: f64,
    lat: f64,
}

impl Step {
    fn from_links(map: &StreetMap, links: &[Link]) -> Vec<Self> {
        let mut current: Option<Self> = None;
        let mut previous_link: Option<&Link> = None;
        let mut steps: Vec<Self> = vec![];
        for link in links.iter() {
            let new_step = Self::from_link(map, &link);
            match current {
                None => {
                    current = Some(new_step);
                }
                Some(step) => {
                    if new_step.street_name == step.street_name {
                        current = Some(Self {
                            distance: step.distance + new_step.distance,
                            ..step
                        });
                    } else {
                        let relative_direction = Self::relative_direction_from_links(
                            &map,
                            &previous_link.unwrap(),
                            &link,
                        );
                        steps.push(step);
                        current = Some(Self {
                            relative_direction,
                            ..new_step
                        });
                    }
                }
            }
            previous_link = Some(link);
        }
        steps.push(current.unwrap());
        steps
    }

    fn from_link(map: &StreetMap, link: &Link) -> Self {
        let (street_name, bogus_name) = match link.name.as_ref() {
            Some(name) => (name.clone(), false),
            None => (format!("{:?}", link.kind).to_lowercase(), true),
        };
        let from = map.point(link.from);
        let to = map.point(link.to);
        let distance = from.distance(&to);
        let bearing = from.bearing(&to);
        Self {
            street_name,
            bogus_name,
            lon: from.longitude,
            lat: from.latitude,
            distance,
            absolute_direction: AbsoluteDirection::from_bearing(bearing),
            relative_direction: RelativeDirection::DEPART,
        }
    }

    fn relative_direction_from_links(map: &StreetMap, a: &Link, b: &Link) -> RelativeDirection {
        let a_from = map.point(a.from);
        let a_to = map.point(a.to);
        let b_to = map.point(b.to);
        let a_bearing = a_from.bearing(&a_to);
        let b_bearing = a_to.bearing(&b_to);
        RelativeDirection::from_bearings(a_bearing, b_bearing)
    }
}

#[derive(Serialize, Copy, Clone)]
#[allow(non_camel_case_types)]
enum RelativeDirection {
    DEPART,
    CONTINUE,
    LEFT,
    SLIGHTLY_LEFT,
    SHARP_LEFT,
    RIGHT,
    SLIGHTLY_RIGHT,
    SHARP_RIGHT,
    UTURN,
}

impl RelativeDirection {
    fn from_bearings(a: f64, b: f64) -> Self {
        let diff = a - b;
        let abs = diff.abs();
        if abs < 15.0 {
            return Self::CONTINUE;
        }
        if diff < 0.0 {
            // right turns
            if abs < 60.0 {
                Self::SLIGHTLY_RIGHT
            } else if abs < 120.0 {
                Self::RIGHT
            } else if abs > 160.0 {
                Self::SHARP_RIGHT
            } else {
                Self::UTURN
            }
        } else {
            // left turns
            if abs < 60.0 {
                Self::SLIGHTLY_LEFT
            } else if abs < 120.0 {
                Self::LEFT
            } else if abs < 160.0 {
                Self::SHARP_LEFT
            } else {
                Self::UTURN
            }
        }
    }
}

#[derive(Serialize, PartialEq, Copy, Clone)]
enum AbsoluteDirection {
    NORTH = 0,
    NORTHEAST = 45,
    EAST = 90,
    SOUTHEAST = 135,
    SOUTH = 180,
    SOUTHWEST = -45,
    WEST = -90,
    NORTHWEST = -135,
}

impl AbsoluteDirection {
    pub fn from_bearing(bearing: f64) -> Self {
        assert!(bearing > -180.0);
        assert!(bearing <= 180.0);
        let bearing = if bearing < 0.0 {
            bearing + 360.0
        } else {
            bearing
        };
        // shift by 22.5 degress to make the comparisons easier
        let r = bearing + 22.5;
        if r < 45.0 {
            Self::NORTH
        } else if r < 90.0 {
            Self::NORTHEAST
        } else if r < 135.0 {
            Self::EAST
        } else if r < 180.0 {
            Self::SOUTHEAST
        } else if r < 225.0 {
            Self::SOUTH
        } else if r < 270.0 {
            Self::SOUTHWEST
        } else if r < 315.0 {
            Self::WEST
        } else if r < 360.0 {
            Self::NORTHWEST
        } else {
            Self::NORTH
        }
    }
}
