# Speedwagon

Work-in-progress OpenTripPlanner-compatible router, written in Rust.

## Trying it out

This assumes you've already set up Rust. If not, try their [Getting Started](https://www.rust-lang.org/learn/get-started) documentation.

```
# get OpenStreetMap PBF for Massachusetts
wget http://download.geofabrik.de/north-america/us/massachusetts-latest.osm.pbf
# get MBTA's GTFS
wget https://cdn.mbta.com/MBTA_GTFS.zip
# Get OpenTripPlanner UI
wget https://github.com/opentripplanner/OpenTripPlanner/archive/dev-1.x.zip
unzip dev-1.x.zip OpenTripPlanner-dev-1.x/src/client/\*
mv OpenTripPlanner-dev-1.x/src/client/ .
# Run the server
cargo run --release -- massachusetts-latest.osm.pbf MBTA_GTFS.zip
```

Then visit [http://localhost:8080/](http://localhost:8080/) to play with it!
